package exercises

import java.util.*

/**
 * Created by fredia on 4/12/16.
 *
 * Implements different types of tree traversal algorithms
 */

fun main(args: Array<String>) {
    val tree1 = Node("2",
            Node("5",
                    Node("10", null, Node("14", null, null)),
                    Node("1", null, null)),
            Node("7",
                    Node("32", null, null),
                    null))

    val tree2 = Node("F",
            Node("B",
                    Node("A"),
                    Node("D",
                            Node("C"),
                            Node("E"))),
            Node("G",
                    null,
                    Node("I",
                            Node("H"),
                            null)))

    println("Preorder 1: ${preOrderTraversal(tree1)}")
    println("Preorder 2: ${preOrderTraversal(tree2)}")

    println("Inorder 1: ${inOrderTraversal(tree1)}")
    println("Inorder 2: ${inOrderTraversal(tree2)}")

    println("Postorder 1: ${postOrderTraversal(tree1)}")
    println("Postorder 2: ${postOrderTraversal(tree2)}")

    println("Levelorder 1: ${levelOrderTraversal(tree1)}")
    println("Levelorder 2: ${levelOrderTraversal(tree2)}")
}

class Node(val payload: String, val left: Node?, val right: Node?){
    constructor(payload: String): this(payload, null, null)
}

/**
 * Display the data for the current node
 * Recursive call on the left subtree
 * Recursive call on the right subtree
 */
fun preOrderTraversal(root: Node): String {
    val output = root.payload.toString() +
            (if (root.left == null) "" else ", ${preOrderTraversal(root.left)}") +
            (if (root.right == null) "" else ", ${preOrderTraversal(root.right)}")
    return output
}

/**
 * Recursive call on the left subtree
 * Display the data for the current node
 * Recursive call on the right subtree
 */
fun inOrderTraversal(root: Node): String {
    val output = (if (root.left == null) "" else "${inOrderTraversal(root.left)}, ") +
            root.payload.toString() +
            (if (root.right == null) "" else ", ${inOrderTraversal(root.right)}")
    return output
}

/**
 * Recursive call on the left subtree
 * Recursive call on the right subtree
 * Display the data for the current node
 */
fun postOrderTraversal(root: Node): String {
    val output = (if (root.left == null) "" else "${postOrderTraversal(root.left)}, ") +
            (if (root.right == null) "" else "${postOrderTraversal(root.right)}, ") +
            root.payload.toString()
    return output
}

/**
 *
 */
fun levelOrderTraversal(root: Node): String {
    val queue = LinkedList<Node>()
    val visited = HashSet<Node>()

    queue.add(root)
    visited.add(root)

    var node = queue.pop()
    var output = ""
    while (node != null) {
        //Print the node data
        output += node.payload.toString() + ", "

        //Add the left and right tree in the queue
        val leftTree = node.left
        if (leftTree != null && !visited.contains(leftTree)) {
            queue.add(leftTree)
            visited.add(leftTree)
        }

        val rightTree = node.right
        if (rightTree != null && !visited.contains(rightTree)) {
            queue.add(rightTree)
            visited.add(rightTree)
        }

        node = queue.poll()
    }

    return output
}