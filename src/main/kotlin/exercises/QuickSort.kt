package exercises

import java.util.*

/**
 * Created by fredia on 4/13/16.
 *
 * Implements the quick sort sorting algorithm
 */
fun main(args: Array<String>){
    //Generate random filled arrays
    val random = Random()
    val testCount = random.nextInt(100)
    for(i in 0 until testCount){
        val arrayLength = random.nextInt(50)
        val testArr = Array<Int>(arrayLength, {random.nextInt(20)})
        quickSort(testArr, 0, arrayLength -1)
        println("Printing test arr #$i out of $testCount: ${Arrays.toString(testArr)}")
    }
}

/**
 * 1. If the array length <= 1, return the array
 * 2. Pick a pivot
 * 3. Partition the array in two with the elements less than the pivot in one partition,
 * and the elements greater than the pivot in the other partition
 */
fun quickSort(input: Array<Int>, start: Int, end: Int) {
    if(start >= end)
        return

    if(start < 0 || end < 0)
        throw IllegalArgumentException("Input bounds should not be negative!")

    //Pick a pivot (i.e: last element)
    val pivot = input[end]
    var pivotIndex = 0

    for(i in 0 until end){
        if(input[i] <= pivot){
            val temp = input[pivotIndex]
            input[pivotIndex] = input[i]
            input[i] = temp
            pivotIndex++
        }
    }

    //Swap the pivot
    input[end] = input[pivotIndex]
    input[pivotIndex] = pivot

    quickSort(input, start, pivotIndex -1)
    quickSort(input, pivotIndex + 1, end)
}
