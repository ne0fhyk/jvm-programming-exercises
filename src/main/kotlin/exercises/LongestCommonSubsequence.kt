package exercises

import java.util.*

/**
 * Created by fredia on 4/14/16.
 */
fun main(args: Array<String>){
    println(Arrays.toString(findLongestCommonSubsequences("banana", "katana")))
    println(Arrays.toString(findLongestCommonSubsequences("gac", "agcat")))
}

fun findLongestCommonSubsequences(lhs: String, rhs: String): Array<String> {
    //If one of the sequence is empty, the lcs is the empty string
    if(lhs.isEmpty() || rhs.isEmpty())
        return arrayOf()

    //Build the table to store the LCS sequence at each step of the calculation
    val lcsTable = Array<Array<LCSSet>>(lhs.length + 1, {i ->
        Array<LCSSet>(rhs.length + 1, {j ->
            LCSSet()
        })
    })

    for(i in 1..lhs.length){
        val lhsChar = lhs[i -1]

        for(j in 1..rhs.length){
            val rhsChar = rhs[j -1]

            val currLcsSet = lcsTable[i][j]

            if(lhsChar == rhsChar){
                //Append the character to the set of lcs obtained so far
                val previousLcsSet = lcsTable[i -1][j - 1]
                val previousSets = previousLcsSet.getSequences()
                if(previousSets.isEmpty()){
                    currLcsSet.addSequence(rhsChar.toString())
                }
                else {
                    for (set in previousLcsSet.getSequences()) {
                        currLcsSet.addSequence(set + rhsChar)
                    }
                }
            }
            else{
                //Take the longest common subsequence between lcsTable[i][j-1] and lcsTable[i-1][j]
                val upLcsSet = lcsTable[i - 1][j]
                val leftLcsSet = lcsTable[i][j -1]
                val longestSetLength = Math.max(upLcsSet.longestSetLength(), leftLcsSet.longestSetLength())

                for(set in upLcsSet.getSequences()){
                    if(set.length >= longestSetLength)
                        currLcsSet.addSequence(set)
                }

                for(set in leftLcsSet.getSequences()){
                    if(set.length >= longestSetLength)
                        currLcsSet.addSequence(set)
                }
            }
        }
    }

    return lcsTable[lhs.length][rhs.length].getSequences()
}

class LCSSet(){
    private var longestSetLength = 0

    private val set = HashSet<String>()

    fun addSequence(sequence: String){
        set.add(sequence)
        longestSetLength = Math.max(longestSetLength, sequence.length)
    }

    fun longestSetLength() = longestSetLength

    fun getSequences() = set.toTypedArray()
}