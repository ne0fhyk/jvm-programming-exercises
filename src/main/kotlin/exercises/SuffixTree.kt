package exercises

import java.util.*

/**
 * Created by fredia on 4/13/16.
 *
 * Implement a suffix tree
 */

fun main(args: Array<String>){
    val suffixTree = SuffixTree()
    suffixTree.addWord("bananas")

    println(suffixTree.containsString("banana"))
    println(suffixTree.containsString("bananas"))
    println(suffixTree.containsString("nana"))
    println(suffixTree.containsString("bnana"))

    println(findLongestCommonSubstring(suffixTree, "bantoo"))
}

fun findLongestCommonSubstring(suffixTree: SuffixTree, input: String): String {
    if(input.isEmpty())
        return input

    val result = StringBuilder()
    var parent = suffixTree.root
    for(character in input){
        val node = parent.getCharacter(character) ?: break
        result.append(character)
        parent = node
    }

    return result.toString()
}

class SuffixTree() {
    class Node(character: Char, val children: HashMap<Char, Node>){

        constructor(character: Char): this(character, HashMap<Char, Node>())

        fun containsCharacter(character: Char) = children.containsKey(character)

        fun containsWord(word: String): Boolean {
            if(word.isEmpty())
                return true

            val targetChild = children[word[0]] ?: return false
            return targetChild.containsWord(word.substring(1))
        }

        fun addCharacter(character: Char): Node {
            val node = Node(character)
            children.put(character, node)
            return node
        }

        fun getCharacter(character: Char) = children[character]
    }

    val root = Node('*', HashMap<Char, Node>())

    fun getCharacterNode(character: Char) = root.getCharacter(character)

    fun addWord(input: String){
        if(input.isEmpty())
            return

        val inputLength = input.length
        for(i in 0 until inputLength){
            //Insert the character at index i as node if it doesn't already exist
            val character = input[i]
            val characterNode = root.getCharacter(character) ?: root.addCharacter(character)

            //Fill the retrieved character node
            var parent = characterNode
            for(j in (i+1) until inputLength){
                val subChar = input[j]

                //Check if the parent already contains a node for the current character
                val current = parent.getCharacter(subChar) ?: parent.addCharacter(subChar)
                parent = current
            }
        }
    }

    fun containsString(input: String) = root.containsWord(input)
}
