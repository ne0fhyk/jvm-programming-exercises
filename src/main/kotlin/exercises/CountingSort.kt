package exercises

import java.util.*

/**
 * Created by fredia on 4/12/16.
 */
fun main(args: Array<String>){
    val input = arrayOf(2, 3, 9, 1, 9, 4, 0, 20, 14, 2, 13)
    println("Result: ${Arrays.toString(countingSort(input, 20))}")
}

private fun countingSort(input: Array<Int>, maxEntry: Int): Array<Int> {
    //Create a fill an histogram for the entries in the input array
    val count = Array(maxEntry + 1, {i -> 0 })

    for(entry in input){
        count[entry] = count[entry] + 1
    }

    //Perform a prefix sum
    var total = 0
    for(index in 0..maxEntry){
        val oldCount = count[index]
        count[index] = total
        total = total + oldCount
    }

    //Fill the output array
    val output = Array(input.size, {i -> 0 })
    for(entry in input){
        output[count[entry]] = entry
        count[entry] += 1
    }

    return output
}