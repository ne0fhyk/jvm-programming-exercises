package exercises;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by fredia on 4/8/16.
 *
 * Given a text of length n, and a screen capable of fitting m characters per line and having k lines,
 * Find out how many times the text of length n can be repeated within the screen.
 *
 * Note:
 * - the text is successfully repeated within the screen if it can fit in its entirety within the remaining space
 * - No words in the text should be spread across multiple lines
 *
 */
public class TextToScreen {

    private static final int[][] screens = {
        {4, 18},
        {2, 10},
        {10, 6},
        {7, 3}
    };

    private static final String[] texts = {
      "Oh my god, they killed kenny",
        "This is yet another test",
        "oh well",
        "a cat is not a dog"
    };

    public static void main(String... args){
        //Test the algorithm across several screens
        for(int[] screen : screens){
            int numLines = screen[0];
            int charsPerLine = screen[1];
            System.out.println("Testing using screen " + numLines + " x " + charsPerLine);
            for(String text: texts){
                System.out.println("Testing with " + text);
                int fitCount = fillScreen(text, numLines, charsPerLine);
                System.out.println("Given text fitted " +fitCount + " times");
                System.out.println();
            }
            System.out.println();
            System.out.println();
        }
    }

    private static int fillScreen(String text, int numLines, int charsPerLine){
        if(text == null || text.isEmpty())
            return -1;

        final int maxCharsOnScreen = numLines * charsPerLine;
        final int textLength = text.length();

        //If the text has more characters than can be fit on the screen
        if(textLength > maxCharsOnScreen)
            return 0;
        else if(textLength == charsPerLine){
            return numLines;
        }
        else{
            //Split the text into words
            String[] words = text.split(" ");
            List<String> wordList = Arrays.asList(words);
            List<String> consumableWords = new ArrayList<>(wordList);
            int fitCount = 0;

            for (int currLine = 0, remainingChars = charsPerLine; currLine < numLines - 1; ) {
                remainingChars = fillLine(consumableWords, remainingChars);

                //Check if the list of consumable words was exhausted
                if(consumableWords.isEmpty()){
                    fitCount++;
                    consumableWords.addAll(wordList);
                }

                if (remainingChars < 0) {
                    //Switch to the next line
                    currLine++;
                    remainingChars = charsPerLine;
                    System.out.println();
                }
            }

            return fitCount;
        }

    }

    private static int fillLine(List<String> words, int remainingChars){
        if(remainingChars < 0)
            return remainingChars;

        for(Iterator<String> it = words.iterator(); it.hasNext(); ){
            //Check if the given word fit within the remaining space
            String word = it.next();
            remainingChars -= word.length();
            if(remainingChars < 0)
                return remainingChars;

            //The word fits. Remove it from the list
            it.remove();
            //Print the word for visualization
            System.out.print(word);

            if(remainingChars > 0){
                //Consume one more character for the space between words.
                remainingChars--;

                //Print the space for visualization
                System.out.print(" ");
            }
        }

        return remainingChars;
    }
}
