package exercises;

import java.util.Arrays;

/**
 * Created by fredia on 4/12/16.
 *
 * Implements the counting sort algorithm
 */
public class CountingSort {

    private static final int[][] inputs = {
        {2, 4, 5, 1, 9, 7, 3},
        {2, 3, 9, 1, 0, 4, 9, 20, 14, 2, 13}
    };

    public static void main(String... args){
        for(int[] input : inputs){
            int[] result = countingSort(input, 20);
            System.out.println("Result: " + Arrays.toString(result));
        }
    }

    private static int[] countingSort(int[] input, int maxEntry){
        //Create and fill the count array
        int[] count = new int[maxEntry + 1];

        //Compute a histogram of the number of times each entry occurs within the input collection
        for(int i = 0; i < input.length; i++){
            count[input[i]]++;
        }

        //Perform a prefix sum computation to determine for each key
        // the starting position in the output array of the items having that key
        for(int i = 0, total = 0; i < maxEntry + 1; i++){
            int oldCount = count[i];
            count[i] = total;
            total += oldCount;
        }

        int[] output = new int[input.length];
        for(int i = 0; i < input.length; i++){
            output[count[input[i]]] = input[i];
            count[input[i]] += 1;
        }

        return output;
    }
}
