package exercises;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fredia on 4/8/16.
 *
 * Write a method that can abbreviate a given word using the following algorithms:
 * 1. if the length of the word is >= 3, replace all the words except the first and last characters
 * with the their count.
 * i.e: word => w2d; fall => f2l; cat => c1t;
 *
 * 2. If the word is of length 1, return 1
 * Otherwise, return the combinations obtained by running the algorithm on each character of the word
 * i.e:
 * a => [1]
 * it => [i1, 1t, 2]
 * cat => [ca1, c1t, c2, 1at, 1a1, 2t, 3]
 *
 */
public class WordAbbreviation {

    private static final String[] words = {
        "cat",
        "word",
        "wood",
        "tennis",
        "human"
    };

    public static void main(String... args){
        System.out.println("Abbrev 1");
        for(String word: words){
            System.out.println(word + " => " + abbrevWord1(word));
        }

        System.out.println();
        System.out.println("Abbrev 2");
        for(String word: words){
            List<String> abbrevs = abbrevWord2(word);
            System.out.println(word + " (" + abbrevs.size() + ") => " + abbrevs);
        }
    }

    /**
     * Complexity is O(2^n)
     */
    private static List<String> abbrevWord2(String word){
        List<String> results = subAbbrevWord2(word);
        results.remove(word);
        return results;
    }

    private static List<String> subAbbrevWord2(String word){
        List<String> results = new ArrayList<>();
        if(word.length() == 1){
            results.add(word);
            results.add("1");
        }
        else{
            String firstChar = String.valueOf(word.charAt(0));

            //Get the abbreviation for the first character
            List<String> firstCharAbbrevs = subAbbrevWord2(firstChar);

            //Get the abbreviation for the rest of the word.
            List<String> remainingAbbrevs = subAbbrevWord2(word.substring(1));

            //Combine the two abbreviation set
            for(String firstCharAbbrev : firstCharAbbrevs){
                boolean isAbbrevDigit = Character.isDigit(firstCharAbbrev.charAt(0));
                for(String remainingAbbrev: remainingAbbrevs){
                    String abbrev;

                    if(isAbbrevDigit && Character.isDigit(remainingAbbrev.charAt(0))){
                        abbrev = String.valueOf(Integer.parseInt(firstCharAbbrev) + Integer.parseInt(remainingAbbrev.substring(0, 1))) + remainingAbbrev.substring(1);
                    }
                    else{
                        abbrev = firstCharAbbrev + remainingAbbrev;
                    }

                    results.add(abbrev);
                }
            }
        }

        return results;
    }

    /**
     * Complexity is O(1) since due to the String class definition, retrieving the length and accessing the
     * characters in the word is a constant time operation.
     * Once that's done, the remaining task is to concatenate three characters.
     *
     */
    private static String abbrevWord1(String word){
        if(word == null || word.length() < 3)
            return word;

        int wordLength = word.length();
        return new StringBuilder().append(word.charAt(0)).append((wordLength - 2)).append(word.charAt(wordLength -1)).toString();
    }
}
