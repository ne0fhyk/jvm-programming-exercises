package exercises;

/**
 * Created by fredia on 4/8/16.
 *
 * 1. Write a method to determine if two binary trees are isomorphic
 * 2. Write a method to determine if two DAG are isomorphic
 * 3. Write a iterator that can iterate through a given DAG
 *
 */
public class DAGIsomorphism {

    private static class Node {
        int payload;
        Node left;
        Node right;

        @Override
        public boolean equals(Object o){
            if(this == o)
                return true;

            if(!(o instanceof Node))
                return false;

            Node that = (Node) o;
            if(this.payload != that.payload){
                return false;
            }

            boolean areLeftSubtreesEqual = this.left == null
                ? that.left == null
                : this.left.equals(that.left);
            if(!areLeftSubtreesEqual)
                return false;

            boolean areRightSubtreesEqual = this.right == null
                ? that.right == null
                : this.right.equals(that.right);
            if(!areRightSubtreesEqual)
                return false;

            return true;
        }
    }

    public static void main(String... args){

    }
}
